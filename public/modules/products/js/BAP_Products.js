var BAP_Products = {

    init: function () {

        this.uploadImage();

    },

    uploadImage: function(){

        $('#image_path').fileinput({
            dropZoneEnabled: false,
            uploadAsync: false,
            showUpload: false,
            showRemove: false,
            showCaption: true,
            maxFileCount: 1,
            showBrowse: true,
            browseOnZoneClick: true,
        });

    },

}

BAP_Products.init();

<?php $__env->startSection('content'); ?>

    <?php echo $dataTable->table(['width' => '100%']); ?>


<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>

<?php echo $dataTable->scripts(); ?>


<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.simple', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo e($entity->name); ?></title>

    <!-- Invoice styling -->
    <style>
        table, tr, td, th, tbody, thead, tfoot {
            page-break-inside: avoid !important;
        }

        body {
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #777;
        }

        body h1 {
            font-weight: 300;
            margin-bottom: 0px;
            padding-bottom: 0px;
            color: #000;
        }

        body h3 {
            font-weight: 300;
            margin-top: 10px;
            margin-bottom: 20px;
            font-style: italic;
            color: #555;
        }

        body a {
            color: #06F;
        }

        .invoice-box {

            margin: auto;

            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 10px;
        }

        .invoice-box table tr.top table td.title .logo {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }

        .title .logo img {
            max-width: 300px;
            max-height: 300px;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }

        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
            padding-left: 10px;
        }

        .invoice-box table tr.details td {
            padding-bottom: 20px;

        }

        .invoice-box table tr.item td {
            border-bottom: 1px solid #eee;
            text-align: left;
            padding-left: 10px;

        }

        .invoice-box table tr.summary td {
            border-bottom: 1px solid #eee;
            text-align: left;
            padding-left: 10px;
        }

        .invoice-box table tr.item.last td {
            border-bottom: none;
        }

        .text-right {
            text-align: right !important;
        }

        .image_path_size{
            max-width: 100px;
        }

    </style>
</head>

<body>

<div class="invoice-box">


    <table cellpadding="0" cellspacing="0">
        <tr class="top">
            <td colspan="4">
                <table>
                    <tr>
                        <td class="title">
                            <div class="logo">
                                <?php if(\Modules\Platform\Core\Helper\CompanySettings::get(\Modules\Platform\Core\Helper\SettingsHelper::S_DISPLAY_SHOW_LOGO_IN_PDF)): ?>
                                    <?php echo \Modules\Platform\Core\Helper\SettingsHelper::pdfLogoPath(); ?>

                                <?php else: ?>
                                    <?php echo e(\Modules\Platform\Core\Helper\CompanySettings::get(\Modules\Platform\Core\Helper\SettingsHelper::S_COMPANY_NAME, config('app.name'))); ?>

                                <?php endif; ?>
                            </div>

                        </td>
                        <td class="text-right">
                            <?php echo e(app('translator')->getFromJson('quotes::quotes.pdf.quote')); ?> #: <?php echo e($entity->id); ?><br>
                            <?php echo e(app('translator')->getFromJson('quotes::quotes.pdf.created')); ?>: <?php echo e(\Modules\Platform\Core\Helper\UserHelper::formatUserDate($entity->created_at)); ?><br>
                            <?php echo e(app('translator')->getFromJson('quotes::quotes.pdf.valid_until')); ?>: <?php echo e(\Modules\Platform\Core\Helper\UserHelper::formatUserDate($entity->valid_unitl)); ?><br>
                            <br/>
                            <b><?php echo e(app('translator')->getFromJson('quotes::quotes.pdf.address')); ?></b><br/>
                            <?php echo e(\Modules\Platform\Core\Helper\CompanySettings::get(\Modules\Platform\Core\Helper\SettingsHelper::S_COMPANY_ADDRESS_)); ?> <br/>
                            <?php echo e(\Modules\Platform\Core\Helper\CompanySettings::get(\Modules\Platform\Core\Helper\SettingsHelper::S_COMPANY_CITY)); ?> <?php echo e(\Modules\Platform\Core\Helper\CompanySettings::get(\Modules\Platform\Core\Helper\SettingsHelper::S_COMPANY_STATE)); ?> <?php echo e(\Modules\Platform\Core\Helper\CompanySettings::get(\Modules\Platform\Core\Helper\SettingsHelper::S_COMPANY_POSTAL_CODE)); ?>


                            <?php echo e(\Modules\Platform\Core\Helper\CompanySettings::get(\Modules\Platform\Core\Helper\SettingsHelper::S_COMPANY_COUNTRY)); ?> <br/>
                            <?php if(!empty(\Modules\Platform\Core\Helper\CompanySettings::get(\Modules\Platform\Core\Helper\SettingsHelper::S_COMPANY_VAT_ID))): ?>
                                <?php echo e(app('translator')->getFromJson('quotes::quotes.pdf.vat')); ?>: <?php echo e(\Modules\Platform\Core\Helper\CompanySettings::get(\Modules\Platform\Core\Helper\SettingsHelper::S_COMPANY_VAT_ID)); ?> <br/>
                            <?php endif; ?>
                            <?php echo e(app('translator')->getFromJson('quotes::quotes.pdf.phone')); ?>: <?php echo e(\Modules\Platform\Core\Helper\CompanySettings::get(\Modules\Platform\Core\Helper\SettingsHelper::S_COMPANY_PHONE)); ?> <br/>
                            <?php echo e(app('translator')->getFromJson('quotes::quotes.pdf.fax')); ?>: <?php echo e(\Modules\Platform\Core\Helper\CompanySettings::get(\Modules\Platform\Core\Helper\SettingsHelper::S_COMPANY_FAX)); ?> <br/>

                        </td>
                    </tr>

                </table>
            </td>
        </tr>

        <tr class="information">
            <td colspan="4">
                <table>
                    <tr>
                        <td>
                            <h2><?php echo e(app('translator')->getFromJson('quotes::quotes.pdf.quotation_for')); ?></h2>
                            <?php if(!empty($entity->account)): ?>
                                <?php echo e($entity->account->name); ?> <br/>
                            <?php endif; ?>
                            <?php echo e($entity->street); ?> <br/>
                            <?php echo e($entity->city); ?>, <?php echo e($entity->zip_code); ?>, <br/>
                            <?php echo e($entity->state); ?> <?php echo e($entity->country); ?> <br/>

                            <?php if(!empty($entity->contact)): ?> <br/>
                            <b><?php echo e(app('translator')->getFromJson('quotes::quotes.pdf.contact')); ?></b> <br/>
                            <?php echo e($entity->contact->full_name); ?> <br/>
                            <?php echo e(app('translator')->getFromJson('quotes::quotes.pdf.email')); ?>: <?php echo e($entity->contact->mobile); ?> <br/>
                            <?php echo e(app('translator')->getFromJson('quotes::quotes.pdf.mobile')); ?>: <?php echo e($entity->contact->email); ?> <br/>
                            <?php echo e(app('translator')->getFromJson('quotes::quotes.pdf.office_phone')); ?>: <?php echo e($entity->contact->phpne); ?>

                            <?php endif; ?>
                        </td>

                        <td>
                            <?php if(!empty($entity->owner)): ?>
                                <h2><?php echo e(app('translator')->getFromJson('quotes::quotes.pdf.sales_representative')); ?></h2>

                                <?php echo e($entity->owner->name); ?> <br/>
                                <?php echo e(app('translator')->getFromJson('quotes::quotes.pdf.email')); ?>: <?php echo e($entity->owner->email); ?> <br/>
                                <?php echo e(app('translator')->getFromJson('quotes::quotes.pdf.mobile')); ?>: <?php echo e($entity->owner->mobile_phone); ?> <br/>
                                <?php echo e(app('translator')->getFromJson('quotes::quotes.pdf.office_phone')); ?>: <?php echo e($entity->owner->office_phone); ?>


                            <?php endif; ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="heading">
            <td>
                <?php echo e(app('translator')->getFromJson('quotes::quotes.pdf.product_service')); ?>
            </td>

            <td>
                <?php echo e(app('translator')->getFromJson('quotes::quotes.pdf.unit_cost')); ?>
            </td>
            <td>
                <?php echo e(app('translator')->getFromJson('quotes::quotes.pdf.quantity')); ?>
            </td>
            <td>
                <?php echo e(app('translator')->getFromJson('quotes::quotes.pdf.line_total')); ?>
            </td>
        </tr>

        <?php $__currentLoopData = $entity->rows; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr class="item">
                <td>
                    <?php echo e($row->product_name); ?>


                    <?php if(config('quotes.show_product_image') && !empty($row->product)): ?>
                        <br />
                        <img src="<?php echo e(public_path($row->product->image_path)); ?>" class="image_path_size" />
                    <?php endif; ?>

                </td>
                <td>
                    <?php echo e(number_format($row->price,2)); ?>

                    <?php if(!empty($entity->currency)): ?>
                        <?php echo e($entity->currency->code); ?>

                    <?php endif; ?>
                </td>
                <td>
                    <?php echo e($row->quantity); ?>

                </td>
                <td>
                    <?php echo e(number_format($row->lineTotal,2)); ?>

                    <?php if(!empty($entity->currency)): ?>
                        <?php echo e($entity->currency->code); ?>

                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <tr class="summary">
            <td colspan="2" rowspan="7">
            </td>
            <td>


                <?php echo e(app('translator')->getFromJson('quotes::quotes.pdf.subtotal')); ?>

            </td>
            <td>
                <?php echo e(number_format($entity->subtotal,2)); ?>

                <?php if(!empty($entity->currency)): ?>
                    <?php echo e($entity->currency->code); ?>

                <?php endif; ?>
            </td>
        </tr>
        <tr class="summary">
            <td>
                <label class=" text-right">
                    <?php echo e(app('translator')->getFromJson('quotes::quotes.pdf.discount')); ?>
                </label>
            </td>
            <td>
                <?php echo e(number_format($entity->discount,2)); ?>

                <?php if(!empty($entity->currency)): ?>
                    <?php echo e($entity->currency->code); ?>

                <?php endif; ?>
            </td>
        </tr>
        <tr class="summary">
            <td>
                <label class=" text-right">
                    <?php echo e(app('translator')->getFromJson('quotes::quotes.pdf.delivery_cost')); ?>
                </label>
            </td>
            <td>
                <?php echo e(number_format($entity->delivery_cost,2)); ?>

                <?php if(!empty($entity->currency)): ?>
                    <?php echo e($entity->currency->code); ?>

                <?php endif; ?>
            </td>
        </tr>
        <tr class="summary">
            <td>
                <label class=" text-right">
                    <?php echo e(app('translator')->getFromJson('quotes::quotes.pdf.tax')); ?>
                    <?php if(!empty($entity->tax)): ?>
                        (<?php echo e($entity->tax->name); ?>)
                    <?php endif; ?>
                </label>
            </td>
            <td>
                <?php echo e(number_format($entity->taxValue,2)); ?>

                <?php if(!empty($entity->currency)): ?>
                    <?php echo e($entity->currency->code); ?>

                <?php endif; ?>
            </td>
        </tr>
        <tr class="summary">
            <td>
                <label class=" text-right">
                    <?php echo e(app('translator')->getFromJson('quotes::quotes.pdf.gross_value')); ?>
                </label>
            </td>
            <td>
                <strong>
                    <?php echo e(number_format($entity->grossValue,2)); ?>

                    <?php if(!empty($entity->currency)): ?>
                        <?php echo e($entity->currency->code); ?>

                    <?php endif; ?>
                </strong>
            </td>
        </tr>

    </table>
    <?php if($entity->shipping): ?>
        <br/><br/>
        <table cellpadding="0" cellspacing="0">
            <tr class="heading">
                <td>
                    <?php echo e(app('translator')->getFromJson('quotes::quotes.pdf.shipping')); ?>
                </td>
            </tr>
            <tr class="item">
                <td>
                    <?php if(!empty($entity->quoteCarrier)): ?>
                        <?php echo e(app('translator')->getFromJson('quotes::quotes.pdf.carrier')); ?>: <?php echo e($entity->quoteCarrier->name); ?>  <br/>
                    <?php endif; ?>
                    <?php echo e($entity->shipping); ?>

                </td>
            </tr>
        </table>
    <?php endif; ?>
    <?php if($entity->notes): ?>
        <br/><br/>
        <table cellpadding="0" cellspacing="0">
            <tr class="heading">
                <td>
                    <?php echo e(app('translator')->getFromJson('quotes::quotes.pdf.notes')); ?>
                </td>
            </tr>
            <tr class="item">
                <td>
                    <?php echo e($entity->notes); ?>

                </td>
            </tr>
        </table>
    <?php endif; ?>
</div>

</html>

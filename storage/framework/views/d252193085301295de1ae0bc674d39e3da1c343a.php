<?php $__env->startSection('content'); ?>

    <div class="block-header">
        <h2><?php echo e(app('translator')->getFromJson('settings::settings.module')); ?></h2>
    </div>

    <div class="row">

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 no-vert-padding">
            <?php echo $__env->make('settings::partial.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>

        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 no-vert-padding">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>

                            <a href="<?php echo e(route('settings.roles.show',$role->id)); ?>" class="btn btn-primary btn-create btn-crud"><?php echo e(app('translator')->getFromJson('user::roles.back')); ?></a>

                            <?php echo e(app('translator')->getFromJson('user::roles.assign_permissions_to')); ?> - <?php echo e($role->display_name); ?>

                            <small><?php echo e(app('translator')->getFromJson('user::roles.module_permissions_description')); ?></small>
                        </h2>
                    </div>
                    <div class="body">

                        <div class="row clearfix">
                            <div class="col-sm-12">

                                <?php echo e(Form::open(['route'=>['settings.roles.permissions-save',$role->id],'method'=>'POST'])); ?>


                                <?php $__currentLoopData = $permissions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $group => $permission): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="col-sm-6">
                                        <h2 class="card-inside-title"><?php echo e($group); ?></h2>
                                        <?php $__currentLoopData = $permission; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $perm): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="switch">
                                                <label>
                                                    <?php echo e(Form::checkbox('permissions[]',strtolower($perm['name']),in_array(strtolower($perm['name']),$rolePermissions)),['id' => strtolower($perm['name'])]); ?>

                                                    <span class="lever switch-col-red">
                                                    </span>
                                                    <?php echo e($perm['name']); ?>

                                                </label>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                <div class="col-sm-12">


                                    <?php echo Form::submit(trans('user::roles.save_permissions'),['class="btn btn-primary m-t-15 waves-effect"']); ?>

                                </div>

                                <?php echo Form::close(); ?>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
    <?php echo JsValidator::formRequest(\Modules\Platform\Settings\Http\Requests\SaveCompanySettingsRequest::class, '#company_settings_form'); ?>

<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
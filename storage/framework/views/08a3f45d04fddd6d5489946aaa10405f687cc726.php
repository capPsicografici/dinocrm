<?php if(isset($passwordForm)): ?>
    <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel"><?php echo e(app('translator')->getFromJson('user::users.change_password')); ?></h4>
                </div>
                <div class="modal-body">
                    <?php echo form($passwordForm); ?>

                </div>
            </div>
        </div>
    </div>

    <?php $__env->startPush('scripts'); ?>
    <?php echo JsValidator::formRequest(\Modules\Platform\User\Http\Requests\UserChangePasswordRequest::class, '#user_change_password'); ?>

    <?php $__env->stopPush(); ?>
<?php endif; ?>
<div class="col-lg-12 col-md-12">
    <h2 class="card-inside-title">
        <?php echo e(app('translator')->getFromJson('invoices::invoices.panel.products_and_services')); ?>

        <div class="section-buttons">
            <a href="#" class="btn normal-text btn-primary btn-xs" id="invoice-add-row">
                <?php echo e(app('translator')->getFromJson('invoices::invoices.form.add_row')); ?>
            </a>
        </div>
    </h2>
</div>

<div class="col-lg-12 col-md-12">
    <div class="table-responsive  col-lg-12 col-md-12 col-sm-12">
        <table id="invoice-rows" class="table table-condensed">
            <thead>

            <th>
                <?php echo e(app('translator')->getFromJson('invoices::invoices.form.product_service')); ?>
            </th>
            <th>
                <?php echo e(app('translator')->getFromJson('invoices::invoices.form.unit_cost')); ?>
            </th>
            <th>
                <?php echo e(app('translator')->getFromJson('invoices::invoices.form.quantity')); ?>
            </th>

            <th>
                <?php echo e(app('translator')->getFromJson('invoices::invoices.form.line_total')); ?>
            </th>
            <th>

            </th>
            </thead>
            <tbody>


            <?php $__currentLoopData = $options['children']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr class="invoice_row">

                    <td>
                        <?php echo form_row($row->id); ?>

                        <div class="input-group">
                        <span class="input-group-addon">
                           <i class="search-product material-icons">search</i>
                        </span>
                            <?php echo form_row($row->product_name); ?>


                        </div>

                    </td>
                    <td>
                        <?php echo form_row($row->price); ?>

                    </td>
                    <td>
                        <?php echo form_row($row->quantity); ?>

                    </td>
                    <td>
                        <?php echo form_row($row->lineTotal); ?>

                    </td>
                    <td>
                        <i class="material-icons invoice-remove-row pointer">clear</i>
                    </td>
                </tr>

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            <tr>
                <td colspan="2" rowspan="7">

                </td>
                <td>
                    <label class="show-control-label text-right">
                        <?php echo e(app('translator')->getFromJson('invoices::invoices.form.subtotal')); ?>
                    </label>
                </td>
                <td class="summary_subtotal">

                </td>
            </tr>
            <tr>
                <td>
                    <label class="show-control-label text-right">
                        <?php echo e(app('translator')->getFromJson('invoices::invoices.form.discount')); ?>
                    </label>
                </td>
                <td class="summary_discount">

                </td>
            </tr>
            <tr>
                <td>
                    <label class="show-control-label text-right">
                        <?php echo e(app('translator')->getFromJson('invoices::invoices.form.delivery_cost')); ?>
                    </label>
                </td>
                <td class="summary_delivery_cost">

                </td>
            </tr>
            <tr>
                <td>
                    <label class="show-control-label text-right">
                        <?php echo e(app('translator')->getFromJson('invoices::invoices.form.tax')); ?>
                    </label>
                </td>
                <td class="summary_tax">

                </td>
            </tr>
            <tr>
                <td>
                    <label class="show-control-label text-right">
                        <?php echo e(app('translator')->getFromJson('invoices::invoices.form.gross_value')); ?>
                    </label>
                </td>
                <td class="summary_gross">

                </td>
            </tr>
            <tr>
                <td>
                    <label class="show-control-label text-right">
                        <?php echo e(app('translator')->getFromJson('invoices::invoices.form.paid_to_date')); ?>
                    </label>
                </td>
                <td class="summary_paid_to_date">

                </td>
            </tr>
            <tr>
                <td>
                    <label class="show-control-label text-right">
                        <?php echo e(app('translator')->getFromJson('invoices::invoices.form.balance_due')); ?>
                    </label>
                </td>
                <td class="summary_balance_due bold">

                </td>
            </tr>

            </tbody>

        </table>
    </div>

</div>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo e($entity->invoice_number); ?></title>

    <!-- Invoice styling -->
    <style>
        table, tr, td, th, tbody, thead, tfoot {
            page-break-inside: avoid !important;
        }

        body {
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #777;
        }

        body h1 {
            font-weight: 300;
            margin-bottom: 0px;
            padding-bottom: 0px;
            color: #000;
        }

        body h3 {
            font-weight: 300;
            margin-top: 10px;
            margin-bottom: 20px;
            font-style: italic;
            color: #555;
        }

        body a {
            color: #06F;
        }

        .invoice-box {

            margin: auto;


            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 10px;
        }

        .invoice-box table tr.top table td.title .logo {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }

        .title .logo img {
            max-width: 300px;
            max-height: 300px;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }

        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
            padding-left: 10px;
        }

        .invoice-box table tr.details td {
            padding-bottom: 20px;

        }

        .invoice-box table tr.item td {
            border-bottom: 1px solid #eee;
            text-align: left;
            padding-left: 10px;

        }

        .invoice-box table tr.summary td {
            border-bottom: 1px solid #eee;
            text-align: left;
            padding-left: 10px;
        }

        .invoice-box table tr.item.last td {
            border-bottom: none;
        }

        .text-right {
            text-align: right !important;
        }

    </style>
</head>

<body>

<div class="invoice-box">


    <table cellpadding="0" cellspacing="0">
        <tr class="top">
            <td colspan="4">
                <table>
                    <tr>
                        <td class="title">
                            <div class="logo">
                                <?php if(\Modules\Platform\Core\Helper\CompanySettings::get(\Modules\Platform\Core\Helper\SettingsHelper::S_DISPLAY_SHOW_LOGO_IN_PDF)): ?>
                                    <?php echo \Modules\Platform\Core\Helper\SettingsHelper::pdfLogoPath(); ?>

                                <?php else: ?>
                                    <?php echo e(\Modules\Platform\Core\Helper\CompanySettings::get(\Modules\Platform\Core\Helper\SettingsHelper::S_COMPANY_NAME, config('app.name'))); ?>

                                <?php endif; ?>
                            </div>

                        </td>
                        <td class="text-right">
                            <?php echo e(app('translator')->getFromJson('invoices::invoices.pdf.invoice_number')); ?> #: <?php echo e($entity->invoice_number); ?><br>
                            <?php echo e(app('translator')->getFromJson('invoices::invoices.pdf.invoice_date')); ?>: <?php echo e(\Modules\Platform\Core\Helper\UserHelper::formatUserDate($entity->invoice_date)); ?><br>
                            <?php echo e(app('translator')->getFromJson('invoices::invoices.pdf.due_date')); ?>: <?php echo e(\Modules\Platform\Core\Helper\UserHelper::formatUserDate($entity->due_date)); ?><br>
                            <br/>
                        </td>
                    </tr>

                </table>
            </td>
        </tr>

        <tr class="information">
            <td colspan="4">
                <table>
                    <tr>
                        <td colspan="2">
                            <h2><?php echo e(app('translator')->getFromJson('invoices::invoices.pdf.company')); ?></h2>
                            <?php echo e($entity->from_company); ?> <br/>
                            <?php if(!empty($entity->from_tax_number)): ?>
                                <?php echo e(app('translator')->getFromJson('invoices::invoices.pdf.tax_number')); ?>: <?php echo e($entity->from_tax_number); ?> <br/>
                            <?php endif; ?>
                            <?php echo e($entity->from_street); ?>, <?php echo e($entity->from_city); ?>, <?php echo e($entity->from_zip_code); ?>, <br/>
                            <?php echo e($entity->from_state); ?> <?php echo e($entity->from_country); ?> <br/>

                        </td>

                    </tr>
                    <tr>
                        <td>
                            <h2><?php echo e(app('translator')->getFromJson('invoices::invoices.pdf.bill_to')); ?></h2>
                            <?php echo e($entity->bill_to); ?> <br/>
                            <?php if(!empty($entity->bill_tax_number)): ?>
                                <?php echo e(app('translator')->getFromJson('invoices::invoices.pdf.tax_number')); ?>: <?php echo e($entity->bill_tax_number); ?> <br/>
                            <?php endif; ?>
                            <?php echo e($entity->bill_street); ?>, <?php echo e($entity->bill_city); ?>, <?php echo e($entity->bill_zip_code); ?>, <br/>
                            <?php echo e($entity->bill_state); ?> <?php echo e($entity->bill_country); ?> <br/>

                        </td>

                        <td >
                            <h2><?php echo e(app('translator')->getFromJson('invoices::invoices.pdf.ship_to')); ?></h2>
                            <?php echo e($entity->ship_to); ?> <br/>
                            <?php if(!empty($entity->ship_tax_number)): ?>
                                <?php echo e(app('translator')->getFromJson('invoices::invoices.pdf.tax_number')); ?>: <?php echo e($entity->ship_tax_number); ?> <br/>
                            <?php endif; ?>
                            <?php echo e($entity->ship_street); ?>, <?php echo e($entity->ship_city); ?>, <?php echo e($entity->ship_zip_code); ?>, <br/>
                            <?php echo e($entity->bill_state); ?> <?php echo e($entity->ship_country); ?> <br/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="heading">
            <td>
                <?php echo e(app('translator')->getFromJson('invoices::invoices.pdf.product_service')); ?>
            </td>

            <td>
                <?php echo e(app('translator')->getFromJson('invoices::invoices.pdf.unit_cost')); ?>
            </td>
            <td>
                <?php echo e(app('translator')->getFromJson('invoices::invoices.pdf.quantity')); ?>
            </td>
            <td>
                <?php echo e(app('translator')->getFromJson('invoices::invoices.pdf.line_total')); ?>
            </td>
        </tr>

        <?php $__currentLoopData = $entity->rows; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr class="item">
                <td>
                    <?php echo e($row->product_name); ?>

                </td>
                <td>
                    <?php echo e(number_format($row->price,2)); ?>

                    <?php if(!empty($entity->currency)): ?>
                        <?php echo e($entity->currency->code); ?>

                    <?php endif; ?>
                </td>
                <td>
                    <?php echo e($row->quantity); ?>

                </td>
                <td>
                    <?php echo e(number_format($row->lineTotal,2)); ?>

                    <?php if(!empty($entity->currency)): ?>
                        <?php echo e($entity->currency->code); ?>

                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <tr class="summary">
            <td colspan="2" rowspan="7">
            </td>
            <td>


                <?php echo e(app('translator')->getFromJson('invoices::invoices.pdf.subtotal')); ?>

            </td>
            <td>
                <?php echo e(number_format($entity->subtotal,2)); ?>

                <?php if(!empty($entity->currency)): ?>
                    <?php echo e($entity->currency->code); ?>

                <?php endif; ?>
            </td>
        </tr>
        <tr class="summary">
            <td>
                <label class=" text-right">
                    <?php echo e(app('translator')->getFromJson('invoices::invoices.pdf.discount')); ?>
                </label>
            </td>
            <td>
                <?php echo e(number_format($entity->discount,2)); ?>

                <?php if(!empty($entity->currency)): ?>
                    <?php echo e($entity->currency->code); ?>

                <?php endif; ?>
            </td>
        </tr>
        <tr class="summary">
            <td>
                <label class=" text-right">
                    <?php echo e(app('translator')->getFromJson('invoices::invoices.pdf.delivery_cost')); ?>
                </label>
            </td>
            <td>
                <?php echo e(number_format($entity->delivery_cost,2)); ?>

                <?php if(!empty($entity->currency)): ?>
                    <?php echo e($entity->currency->code); ?>

                <?php endif; ?>
            </td>
        </tr>
        <tr class="summary">
            <td>
                <label class=" text-right">
                    <?php echo e(app('translator')->getFromJson('invoices::invoices.pdf.tax')); ?>
                    <?php if(!empty($entity->tax)): ?>
                        (<?php echo e($entity->tax->name); ?>)
                    <?php endif; ?>
                </label>
            </td>
            <td>
                <?php echo e(number_format($entity->taxValue,2)); ?>

                <?php if(!empty($entity->currency)): ?>
                    <?php echo e($entity->currency->code); ?>

                <?php endif; ?>
            </td>
        </tr>
        <tr class="summary">
            <td>
                <label class=" text-right">
                    <?php echo e(app('translator')->getFromJson('invoices::invoices.pdf.gross_value')); ?>
                </label>
            </td>
            <td>
                <strong>
                    <?php echo e(number_format($entity->grossValue,2)); ?>

                    <?php if(!empty($entity->currency)): ?>
                        <?php echo e($entity->currency->code); ?>

                    <?php endif; ?>
                </strong>
            </td>
        </tr>


    </table>
    <?php if($entity->shipping): ?>
        <br/><br/>
        <table cellpadding="0" cellspacing="0">
            <tr class="heading">
                <td>
                    <?php echo e(app('translator')->getFromJson('invoices::invoices.pdf.shipping')); ?>
                </td>
            </tr>
            <tr class="item">
                <td>
                    <?php if(!empty($entity->quoteCarrier)): ?>
                        <?php echo e(app('translator')->getFromJson('invoices::invoices.pdf.carrier')); ?>: <?php echo e($entity->quoteCarrier->name); ?>  <br/>
                    <?php endif; ?>
                    <?php echo e($entity->shipping); ?>

                </td>
            </tr>
        </table>
    <?php endif; ?>
    <?php if($entity->terms_and_cond): ?>
        <br/><br/>
        <table cellpadding="0" cellspacing="0">
            <tr class="heading">
                <td>
                    <?php echo e(app('translator')->getFromJson('invoices::invoices.pdf.terms_and_cond')); ?>
                </td>
            </tr>
            <tr class="item">
                <td>
                    <?php echo e($entity->terms_and_cond); ?>

                </td>
            </tr>
        </table>
    <?php endif; ?>
    <?php if($entity->notes): ?>
        <br/><br/>
        <table cellpadding="0" cellspacing="0">
            <tr class="heading">
                <td>
                    <?php echo e(app('translator')->getFromJson('invoices::invoices.pdf.notes')); ?>
                </td>
            </tr>
            <tr class="item">
                <td>
                    <?php echo e($entity->notes); ?>

                </td>
            </tr>
        </table>
    <?php endif; ?>
</div>

</html>

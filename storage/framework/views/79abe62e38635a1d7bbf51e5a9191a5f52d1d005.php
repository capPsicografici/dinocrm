<?php $__env->startSection('content'); ?>

    <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>

                            <div class="header-buttons">
                                <?php if($settingsPermission != '' && Auth::user()->hasPermissionTo($settingsPermission)): ?>
                                    <?php if(count($moduleSettingsLinks) > 0 ): ?>
                                    <div class="btn-group btn-crud pull-right">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <?php echo e(app('translator')->getFromJson('core::core.settings')); ?> <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <?php $__currentLoopData = $moduleSettingsLinks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $link): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                             <li>
                                                 <a href="<?php echo e(route($link['route'])); ?>"><?php echo e(trans($language_file.'.'.$link['label'])); ?></a>
                                             </li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                        </ul>
                                    </div>
                                    <?php endif; ?>
                                <?php endif; ?>

                                <?php if(!empty($indexActionButtons)): ?>
                                    <div class="btn-group btn-crud pull-right">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <?php echo e(app('translator')->getFromJson('core::core.more')); ?> <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <?php $__currentLoopData = $indexActionButtons; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $link): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li>
                                                    <?php echo e(Html::link($link['href'],$link['label'],$link['attr'])); ?>

                                                </li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                    </div>
                                <?php endif; ?>



                                <?php if($permissions['create'] == '' or Auth::user()->hasPermissionTo($permissions['create'])): ?>
                                    <a href="<?php echo e(route($routes['create'])); ?>" title="<?php echo e(app('translator')->getFromJson('core::core.crud.create')); ?>" class="btn btn-primary btn-create btn-crud"><?php echo e(app('translator')->getFromJson('core::core.crud.create')); ?></a>
                                <?php endif; ?>
                                <?php if($settingsBackRoute != ''): ?>
                                    <a href="<?php echo e(route($settingsBackRoute)); ?>" title="<?php echo e(app('translator')->getFromJson('core::core.crud.back')); ?>" class="btn btn-default btn-crud"><?php echo e(app('translator')->getFromJson('core::core.crud.back')); ?></a>
                                <?php endif; ?>
                            </div>
                            <div class="header-text">



                                <?php echo e(app('translator')->getFromJson($language_file.'.module')); ?>  - <?php echo e(app('translator')->getFromJson('core::core.crud.list')); ?>
                                <small><?php echo e(app('translator')->getFromJson($language_file.'.module_description')); ?></small>
                            </div>

                        </h2>
                    </div>
                    <div class="body">

                        <div class="table-responsive  col-lg-12 col-md-12 col-sm-12">
                            <?php echo $dataTable->table(['width' => '100%']); ?>

                        </div>

                        <div data-create-route="<?php echo e(route($routes['create'],['mode'=>'modal'])); ?>" id="modal_quick_create" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                                        <h4 class="modal-title"><?php echo e(app('translator')->getFromJson($language_file.'.module')); ?> - <?php echo e(app('translator')->getFromJson('core::core.crud.create')); ?></h4>

                                    </div>
                                    <div class="modal-body">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php echo $__env->make('core::crud.partial.csv_import', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                    </div>
                </div>


            </div>

    </div>





<?php $__env->stopSection(); ?>

<?php $__env->startPush('css'); ?>
    <?php $__currentLoopData = $cssFiles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <link rel="stylesheet" href="<?php echo Module::asset($moduleName.':css/'.$file); ?>"></link>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php $__env->stopPush(); ?>

<?php $__env->startPush('scripts'); ?>
    <?php $__currentLoopData = $jsFiles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jsFile): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <script src="<?php echo Module::asset($moduleName.':js/'.$jsFile); ?>"></script>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php $__env->stopPush(); ?>

<?php $__env->startPush('scripts'); ?>
    <?php echo $dataTable->scripts(); ?>

<?php $__env->stopPush(); ?>



<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
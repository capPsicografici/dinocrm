<?php $__env->startSection('content'); ?>

    <div class="block-header">
        <h2><?php echo e(app('translator')->getFromJson('settings::settings.module')); ?></h2>
    </div>

    <div class="row">

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 no-vert-padding">

            <?php echo $__env->make('settings::partial.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>

        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 no-vert-padding">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>

                            <div class="btn-group next-prev-btn-group" role="group">
                                <?php if($prev_record): ?>
                                    <a href="<?php echo e(route($routes['show'],$prev_record)); ?>"
                                       title="<?php echo e(app('translator')->getFromJson('core::core.crud.prev')); ?>"
                                       class="btn btn-primary waves-effect btn-crud btn-prev"><?php echo e(app('translator')->getFromJson('core::core.crud.prev')); ?></a>
                                <?php endif; ?>

                                <?php if($next_record): ?>
                                    <a href="<?php echo e(route($routes['show'],$next_record)); ?>"
                                       title="<?php echo e(app('translator')->getFromJson('core::core.crud.next')); ?>"
                                       class="btn btn-primary waves-effect btn-crud btn-next"><?php echo e(app('translator')->getFromJson('core::core.crud.next')); ?></a>
                                <?php endif; ?>
                            </div>

                            <a href="<?php echo e(route($routes['index'])); ?>" title="<?php echo e(app('translator')->getFromJson('core::core.crud.back')); ?>"
                               class="btn btn-primary waves-effect btn-back btn-crud"><?php echo e(app('translator')->getFromJson('core::core.crud.back')); ?></a>

                            <a href="<?php echo e(route($routes['edit'],$entity)); ?>"
                               class="btn btn-primary waves-effect btn-edit btn-crud"><?php echo e(app('translator')->getFromJson($language_file.'.edit')); ?></a>

                            <?php echo Form::open(['route' => [$routes['destroy'], $entity], 'method' => 'delete']); ?>


                            <?php echo Form::button(trans('core::core.crud.delete'), [ 'type' => 'submit', 'class' => 'btn btn-danger waves-effect btn-edit btn-crud', 'onclick' => "return confirm($.i18n._('are_you_sure'))" ]); ?>


                            <?php echo Form::close(); ?>




                            <?php echo e(app('translator')->getFromJson($language_file.'.module')); ?> - <?php echo e(app('translator')->getFromJson($language_file.'.details')); ?>
                            <small><?php echo e(app('translator')->getFromJson($language_file.'.module_description')); ?></small>

                        </h2>

                    </div>
                    <div class="body">
                        <div class="row">

                            <div class="col-lg-12 col-md-12">
                                <?php $__currentLoopData = $customShowButtons; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $btn): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php echo Html::customButton($btn); ?>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>


                            <?php $__currentLoopData = $show_fields; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $panelName => $panel): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php echo e(Html::section($language_file,$panelName)); ?>



                                    <?php $__currentLoopData = $panel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fieldName => $options): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if (\Illuminate\Support\Facades\Blade::check('fieldPermission', $options)): ?>
                                        <?php echo e(Html::renderField($entity,$fieldName,$options,$language_file)); ?>

                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <?php $__currentLoopData = $includeViews; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php echo $__env->make($v, array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
    <?php $__currentLoopData = $jsFiles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jsFile): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <script src="<?php echo Module::asset($moduleName.':js/'.$jsFile); ?>"></script>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php $__env->stopPush(); ?>



<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('content'); ?>

    <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>

                            <div class="header-buttons">
                                <a href="<?php echo e(route($routes['show'],$entity)); ?>" title="<?php echo e(app('translator')->getFromJson('core::core.crud.back')); ?>" class="btn btn-primary btn-back btn-crud"><?php echo e(app('translator')->getFromJson('core::core.crud.back')); ?></a>
                            </div>
                            <div class="header-text">
                                <?php echo e(app('translator')->getFromJson($language_file.'.module')); ?>  - <?php echo e(app('translator')->getFromJson('core::core.crud.edit')); ?>
                                <small><?php echo e(app('translator')->getFromJson($language_file.'.module_description')); ?></small>
                            </div>
                        </h2>


                    </div>
                    <div class="body">
                        <div class="row">


                            <?php echo form_start($form); ?>


                            <?php $__currentLoopData = $show_fields; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $panelName => $panel): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <?php echo e(Html::section($language_file,$panelName,$sectionButtons)); ?>



                                    <?php $__currentLoopData = $panel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fieldName => $options): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                        <?php if(!isset($options['hide_in_form'])): ?>
                                            <?php if($loop->iteration % 2 == 0): ?>
                                                <div class="<?php echo e(isset($options['col-class']) ? $options['col-class'] : 'col-lg-6 col-md-6 col-sm-6'); ?>">
                                            <?php else: ?>
                                                <div class="<?php echo e(isset($options['col-class']) ? $options['col-class'] : 'col-lg-6 col-md-6 col-sm-6 clear-left'); ?>">
                                            <?php endif; ?>

                                               <?php echo form_row($form->{$fieldName}); ?>

                                            </div>
                                        <?php endif; ?>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>



                            <?php echo form_end($form, $renderRest = true); ?>


                    </div>

                        </div>
                </div>
            </div>
        </div>

        <?php $__currentLoopData = $includeViews; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php echo $__env->make($v, array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>



        <?php $__env->stopSection(); ?>


        <?php $__env->startPush('css'); ?>
            <?php $__currentLoopData = $cssFiles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <link rel="stylesheet" href="<?php echo Module::asset($moduleName.':css/'.$file); ?>"></link>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php $__env->stopPush(); ?>

<?php $__env->startPush('scripts'); ?>
    <?php $__currentLoopData = $jsFiles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jsFile): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <script src="<?php echo Module::asset($moduleName.':js/'.$jsFile); ?>"></script>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php $__env->stopPush(); ?>



<?php if($form_request != null ): ?>
    <?php $__env->startPush('scripts'); ?>
        <?php echo JsValidator::formRequest($form_request, '#module_form'); ?>

    <?php $__env->stopPush(); ?>
<?php endif; ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
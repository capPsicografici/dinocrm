<?php $__env->startSection('content'); ?>

    <div class="block-header">
        <h2><?php echo e(app('translator')->getFromJson('account::account.module')); ?> - <?php echo e(Auth::user()->name); ?></h2>
    </div>

    <div class="row">

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card card-account">


                <div class="body">

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <ul class="nav nav-tabs tab-nav-right" role="tablist">

                                <li role="presentation" class="active">
                                    <a href="#preferences"
                                       data-toggle="tab"><?php echo e(app('translator')->getFromJson('account::account.menu.preferences')); ?></a>
                                </li>

                                <li role="presentation">
                                    <a href="#password"
                                       data-toggle="tab"><?php echo e(app('translator')->getFromJson('account::account.menu.change_password')); ?></a>
                                </li>

                            </ul>
                            <div class="tab-content">

                                <div role="tabpanel" class="tab-pane fade in active" id="preferences">

                                    <div class="in-account">

                                        <?php echo $__env->make('account::partial.update-account', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane fade" id="password">

                                    <div class="in-account">
                                        <?php echo $__env->make('account::partial.change-password', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
    <?php echo $dataTable->scripts(); ?>

<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('content'); ?>

    <div class="block-header">
        <h2><?php echo e(app('translator')->getFromJson('settings::settings.module')); ?></h2>
    </div>

    <div class="row">

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 no-vert-padding">

            <?php echo $__env->make('settings::partial.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>

        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 no-vert-padding">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">

                    <div class="body">

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <?php echo app('arrilot.widget')->run('Modules\Platform\Settings\Widgets\UserCountWidget',['count_active' =>
                            true,'color'=>'bg-light-green']); ?>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <?php echo app('arrilot.widget')->run('Modules\Platform\Settings\Widgets\UserCountWidget',['count_active' =>
                            false,'widget_title'=>'inactive','color'=>'bg-deep-orange']); ?>
                        </div>

                        <?php if($company != null ): ?>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="info-box">
                                    <div class="icon bg-light-blue">
                                        <i class="material-icons ">person</i>
                                    </div>
                                    <div class="content">
                                        <div class="text"><?php echo e(app('translator')->getFromJson('settings::settings.users_limit')); ?></div>
                                        <div class="number"><?php echo e($currentUsers); ?> / <?php echo $company->user_limit != null ? $company->user_limit : '&#x221e;'; ?> </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="info-box">
                                    <div class="icon bg-purple">
                                        <i class="material-icons ">cloud_upload</i>
                                    </div>
                                    <div class="content">
                                        <div class="text"><?php echo e(app('translator')->getFromJson('settings::settings.storage_limit')); ?></div>
                                        <div class="number"><?php echo e($companyFileSize); ?> / <?php echo $company->storage_limit != null ? $company->storage_limit : '&#x221e;'; ?> gb </div>
                                    </div>
                                </div>
                            </div>

                        <?php endif; ?>


                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
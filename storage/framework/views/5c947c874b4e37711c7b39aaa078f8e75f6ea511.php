<?php $__env->startSection('content'); ?>


    <div class="row">

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card card-account">

                <div class="header">
                    <h2>

                        <div class="header-text">
                            <?php echo e(app('translator')->getFromJson('notifications::notifications.user_notifications')); ?>
                            <small><?php echo e(app('translator')->getFromJson('notifications::notifications.module_description')); ?></small>
                        </div>

                    </h2>


                </div>
                <div class="body">
                            <div id="notification-scroll" data-ui="jscroll-default">
                                <ul class="ul-list-item">
                                    <?php if($userNotifications->count() == 0): ?>
                                        <li>
                                            <h4 class="text-center"><?php echo e(app('translator')->getFromJson('notifications::notifications.no_new_notifications')); ?></h4>
                                            <p class="text-center big-icon col-blue-grey">
                                                <i class="material-icons big-icon">notifications_none</i>
                                            </p>
                                        </li>
                                    <?php endif; ?>
                                    <?php $__currentLoopData = $userNotifications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notification): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if(isset($notification->data['content'])): ?>
                                            <li>

                                                <div class="msg waves-effect waves-block">
                                                    <a title="<?php echo e(app('translator')->getFromJson('notifications::notifications.delete_notification')); ?>" data-id="<?php echo e($notification->id); ?>" href="#" class="btn-xs pull-right waves-effect waves-block notification-delete">
                                                        <i class="material-icons col-blue-grey">delete</i>
                                                    </a>
                                                    <a data-id="<?php echo e($notification->id); ?>" href="#" class="btn-xs pull-right waves-effect waves-block notification-mark">
                                                        <i class="material-icons col-blue-grey">fiber_manual_record</i>
                                                    </a>
                                                    <div class="icon-circle <?php echo e($notification->data['color']); ?>">
                                                        <i class="material-icons"><?php echo e($notification->data['icon']); ?></i>
                                                    </div>
                                                    <a href="<?php echo e($notification->data['url'] != '' ? $notification->data['url'] : '#'); ?>" class="info info-90">

                                                        <span style="display: <?php echo e(empty($notification->read_at) ? 'block': 'none'); ?>" class="badge bg-pink pull-right"><?php echo e(app('translator')->getFromJson('core::core.new')); ?></span>

                                                        <h4><?php echo e($notification->data['content']); ?></h4>
                                                        <p>
                                                            <i class="material-icons">access_time</i> <?php echo e(\Modules\Platform\Core\Helper\UserHelper::formatUserDateTime($notification->created_at)); ?>

                                                        </p>
                                                        <?php if(isset($notification->data['more_content'])): ?>
                                                            <p><?php echo e($notification->data['more_content']); ?></p>
                                                        <?php endif; ?>

                                                    </a>
                                                </div>
                                            </li>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>

                                <div class="notifications-pagination">
                                    <?php echo e($userNotifications->links()); ?>

                                </div>

                            </div>

                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
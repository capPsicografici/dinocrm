<?php echo Form::open(['route' => [$unlink_route], 'method' => 'post','class'=>'unlink-relation']); ?>


<?php echo e(Form::hidden('entityId',$entityId)); ?>

<?php echo e(Form::hidden('relationEntityId',$relationEntityId)); ?>


<?php echo Form::button('<i class="unlink-icon material-icons">clear</i>', [
    'type' => 'submit',
    'class' => 'unlink-relation',
    'title' => trans('core::core.unlink_relation'),
    'onclick' => "return confirm($.i18n._('are_you_sure'))"
]); ?>


<?php echo Form::close(); ?>


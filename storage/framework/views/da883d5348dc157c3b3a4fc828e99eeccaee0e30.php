
<?php if(isset($btn['href'])): ?>
    <?php echo link_to($btn['href'],$btn['label'],$btn['attr']); ?>

<?php else: ?>
    <?php echo Form::button($btn['label'], $btn['attr']); ?>

<?php endif; ?>

<div class="table-responsive col-lg-12 col-md-12">
<table class="table table-striped table-condensed">
    <tr>
        <th><?php echo e(app('translator')->getFromJson('core::core.table.updated_by')); ?></th>
        <th colspan="2"><?php echo e($entity->causer->name); ?> <?php echo e(app('translator')->getFromJson('core::core.table.@')); ?> <?php echo e(\Modules\Platform\Core\Helper\UserHelper::formatUserDateTime($entity->created_at)); ?></th>
    </tr>

    <?php if(count($entity->changes()) > 0 ): ?>

        <tr>
            <th>
                <?php echo e(trans('core::core.table.activity_log_property')); ?>

            </th>


            <th>
                <?php echo e(trans('core::core.table.activity_log_old')); ?>

            </th>

            <th>
                <?php echo e(trans('core::core.table.activity_log_after')); ?>

            </th>
        </tr>
        <?php $__currentLoopData = $entity->changes()->get('attributes'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td>
                    <?php echo e($key); ?>

                </td>

                <td>
                    <?php echo e($entity->changes()->get('old')[$key]); ?>



                </td>
                <td>
                    <?php if($value != $entity->changes()->get('old')[$key] ): ?>
                        <span class="color-red">
                              <?php echo e($value); ?>

                        </span>
                    <?php else: ?>
                        <?php echo e($value); ?>

                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    <?php endif; ?>


</table>
</div>


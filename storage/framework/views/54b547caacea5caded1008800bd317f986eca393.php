<div class="col-lg-12 col-md-12">
    <h2 class="card-inside-title"><?php echo e(app('translator')->getFromJson('invoices::invoices.panel.products_and_services')); ?></h2>
</div>


<div class="col-lg-12 col-md-12">
    <div class="table-responsive  col-lg-12 col-md-12 col-sm-12">
        <table class="table  table-condensed">
            <thead>
            <th>
                <?php echo e(app('translator')->getFromJson('invoices::invoices.form.product_service')); ?>
            </th>
            <th>
                <?php echo e(app('translator')->getFromJson('invoices::invoices.form.unit_cost')); ?>
            </th>
            <th>
                <?php echo e(app('translator')->getFromJson('invoices::invoices.form.quantity')); ?>
            </th>

            <th>
                <?php echo e(app('translator')->getFromJson('invoices::invoices.form.line_total')); ?>
            </th>
            </thead>
            <tbody>

            <?php $__currentLoopData = $entity->rows; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td>
                        <?php echo e($row->product_name); ?>

                    </td>
                    <td>
                        <?php echo e(number_format($row->price,2)); ?>

                        <?php if(!empty($entity->currency)): ?>
                            <?php echo e($entity->currency->code); ?>

                        <?php endif; ?>
                    </td>
                    <td>
                        <?php echo e($row->quantity); ?>

                    </td>
                    <td>
                        <?php echo e(number_format($row->lineTotal,2)); ?>

                        <?php if(!empty($entity->currency)): ?>
                            <?php echo e($entity->currency->code); ?>

                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td colspan="2" rowspan="7">

                </td>
                <td>
                    <label class="show-control-label text-right">
                        <?php echo e(app('translator')->getFromJson('invoices::invoices.form.subtotal')); ?>
                    </label>
                </td>
                <td>
                    <?php echo e(number_format($entity->subtotal,2)); ?>

                    <?php if(!empty($entity->currency)): ?>
                        <?php echo e($entity->currency->code); ?>

                    <?php endif; ?>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="show-control-label text-right">
                        <?php echo e(app('translator')->getFromJson('invoices::invoices.form.discount')); ?>
                    </label>
                </td>
                <td>
                    <?php echo e(number_format($entity->discount,2)); ?>

                    <?php if(!empty($entity->currency)): ?>
                        <?php echo e($entity->currency->code); ?>

                    <?php endif; ?>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="show-control-label text-right">
                        <?php echo e(app('translator')->getFromJson('invoices::invoices.form.delivery_cost')); ?>
                    </label>
                </td>
                <td>
                    <?php echo e(number_format($entity->delivery_cost,2)); ?>

                    <?php if(!empty($entity->currency)): ?>
                        <?php echo e($entity->currency->code); ?>

                    <?php endif; ?>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="show-control-label text-right">
                        <?php echo e(app('translator')->getFromJson('invoices::invoices.form.tax')); ?>
                    </label>
                </td>
                <td>
                    <?php echo e(number_format($entity->taxValue,2)); ?>

                    <?php if(!empty($entity->currency)): ?>
                        <?php echo e($entity->currency->code); ?>

                    <?php endif; ?>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="show-control-label text-right">
                        <?php echo e(app('translator')->getFromJson('invoices::invoices.form.gross_value')); ?>
                    </label>
                </td>
                <td>
                    <strong>
                        <?php echo e(number_format($entity->grossValue,2)); ?>

                        <?php if(!empty($entity->currency)): ?>
                            <?php echo e($entity->currency->code); ?>

                        <?php endif; ?>
                    </strong>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="show-control-label text-right">
                        <?php echo e(app('translator')->getFromJson('invoices::invoices.form.paid_to_date')); ?>
                    </label>
                </td>
                <td>
                    <strong>
                        <?php echo e(number_format($entity->paid,2)); ?>

                        <?php if(!empty($entity->currency)): ?>
                            <?php echo e($entity->currency->code); ?>

                        <?php endif; ?>
                    </strong>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="show-control-label text-right">
                        <?php echo e(app('translator')->getFromJson('invoices::invoices.form.balance_due')); ?>
                    </label>
                </td>
                <td>
                    <strong>
                        <?php echo e(number_format($entity->balanceDue,2)); ?>

                        <?php if(!empty($entity->currency)): ?>
                            <?php echo e($entity->currency->code); ?>

                        <?php endif; ?>
                    </strong>
                </td>
            </tr>

            </tbody>

        </table>
    </div>
</div>
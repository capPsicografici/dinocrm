<?php
return [
    'name' => 'Leads',

    'entity_private_access' => true,

    /**
     * Always use lower name without custom characters, spaces, etc
     */
    'permissions' => [
        'leads.settings',
        'leads.browse',
        'leads.create',
        'leads.update',
        'leads.destroy'
    ]

];

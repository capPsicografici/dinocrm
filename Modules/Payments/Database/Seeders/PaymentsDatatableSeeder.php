<?php

namespace Modules\Payments\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Platform\Core\Helper\SeederHelper;

class PaymentsDatatableSeeder extends SeederHelper
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $statusData = [
            ['id' => 1, 'name' => 'submitted'],
            ['id' => 2, 'name' => 'approved'],
            ['id' => 3, 'name' => 'declined'],
        ];

        DB::table('payments_dict_status')->truncate();

        $this->saveOrUpdate('payments_dict_status', $statusData);

        $categoryData = [
            ['id' => 1, 'name' => 'gas'],
            ['id' => 2, 'name' => 'travel'],
            ['id' => 3, 'name' => 'meals'],
            ['id' => 4, 'name' => 'car_rental'],
            ['id' => 5, 'name' => 'cell_phone'],
            ['id' => 6, 'name' => 'groceries'],
            ['id' => 7, 'name' => 'invoice'],
        ];

        DB::table('payments_dict_category')->truncate();

        $this->saveOrUpdate('payments_dict_category', $categoryData);

        $paymentMethodData = [
            ['id' => 1, 'name' => 'cash'],
            ['id' => 2, 'name' => 'cheque'],
            ['id' => 3, 'name' => 'credit_card'],
            ['id' => 4, 'name' => 'direct_debit'],
        ];

        DB::table('payments_dict_payment_method')->truncate();

        $this->saveOrUpdate('payments_dict_payment_method', $paymentMethodData);
    }
}

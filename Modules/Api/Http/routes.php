<?php


Route::group(['middleware' => 'web', 'prefix' => 'api', 'namespace' => 'Modules\Api\Http\Controllers'], function () {

    Route::any('login', 'ApiAuthController@login');

    Route::group(['prefix' => 'saas'], function (){
        Route::post('/register','Saas\SaasApiController@registerCompany');

        Route::post('/deactivate-account','Saas\SaasApiController@deactivateAccount');

        Route::post('/resume-account','Saas\SaasApiController@resumeAccount');

        Route::post('/update-plan','Saas\SaasApiController@updatePlan');

    });

    Route::group(['prefix' => 'leads'], function (){
        Route::get('/','LeadsApiController@index');
        Route::get('get/{id}','LeadsApiController@get');
        Route::post('create','LeadsApiController@store');
        Route::post('update/{id}','LeadsApiController@update');
        Route::delete('delete/{id}','LeadsApiController@destroy');
    });

    Route::group(['prefix' => 'contacts'], function (){
        Route::get('/','ContactsApiController@index');
        Route::get('get/{id}','ContactsApiController@get');
        Route::post('create','ContactsApiController@store');
        Route::post('update/{id}','ContactsApiController@update');
        Route::delete('delete/{id}','ContactsApiController@destroy');
    });

    Route::group(['prefix' => 'tickets'], function (){
        Route::get('/','TicketsApiController@index');
        Route::get('get/{id}','TicketsApiController@get');
        Route::post('create','TicketsApiController@store');
        Route::post('update/{id}','TicketsApiController@update');
        Route::delete('delete/{id}','TicketsApiController@destroy');
    });

});

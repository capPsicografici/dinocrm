<?php

namespace Modules\Accounts\Datatables\Tabs;

use Modules\Deals\Entities\Deal;
use Modules\Platform\Core\Datatable\RelationDataTable;
use Modules\Platform\Core\Helper\DataTableHelper;
use Modules\Tickets\Entities\Ticket;
use Yajra\DataTables\EloquentDataTable;

/**
 * Class AccountDealsDatatable
 * @package Modules\Accounts\Datatables\Tabs
 */
class AccountDealsDatatable extends RelationDataTable
{
    const SHOW_URL_ROUTE = 'deals.deals.show';

    protected $unlinkRoute = 'accounts.deals.unlink';

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        $this->applyLinks($dataTable, self::SHOW_URL_ROUTE, 'deals_');

        $dataTable->filterColumn('owner', function ($query, $keyword) {
            DataTableHelper::queryOwner($query, $keyword);
        });

        $dataTable->filterColumn('created_at', function ($query, $keyword) {
            $dates = DataTableHelper::getDatesForFilter($keyword);

            if ($dates != null) {
                $query->whereBetween('created_at', array($dates[0], $dates[1]));
            }
        });
        $dataTable->filterColumn('updated_at', function ($query, $keyword) {
            $dates = DataTableHelper::getDatesForFilter($keyword);

            if ($dates != null) {
                $query->whereBetween('updated_at', array($dates[0], $dates[1]));
            }
        });

        return $dataTable;
    }

    /**
     * Get query source of dataTable.
     *
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Deal $model)
    {
        return $model->with('owner')->newQuery()->select();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('AccountDealsDatatable' . $this->tableSuffix)
            ->columns($this->getColumns())
            ->minifiedAjax(route($this->route, ['entityId' => $this->entityId]))
            ->setTableAttribute('class', 'table table-hover')
            ->parameters([
                'dom' => 'lBfrtip',
                'responsive' => false,
                'stateSave' => true,
                'columnFilters' => [
                    [
                        'column_number' => $this->countFilterColumn(0),
                        'filter_type' => 'text'
                    ],
                    [
                        'column_number' => $this->countFilterColumn(1),
                        'filter_type' => 'bap_date_range_picker',

                    ],
                    [
                        'column_number' => $this->countFilterColumn(2),
                        'filter_type' => 'bap_date_range_picker',
                    ],
                    [
                        'column_number' => $this->countFilterColumn(3),
                        'filter_type' => 'select',
                        'select_type' => 'select2',
                        'select_type_options' => [
                            'theme' => "bootstrap",
                            'width' => '100%'
                        ],
                        'data' => DataTableHelper::filterOwnerDropdown()
                    ]
                ],
                'buttons' => DataTableHelper::buttons(),
                'regexp' => true

            ]);
    }

    /**
     * @return array
     */
    protected function getColumns()
    {
        $unlink = [
            'unlink' => [
                'data' => 'unlink',
                'title' => '',
                'data_type' => 'unlink',
                'orderable' => false,
                'searchable' => false,

            ]
        ];

        $check_selection = [
            'check_select' => [
                'data' => 'check_select',
                'title' => '',
                'data_type' => 'check_select',
                'orderable' => false,
                'searchable' => false,
            ]
        ];

        $columns =
            [
                'name' => [
                    'data' => 'name',
                    'title' => trans('core::core.table.name'),
                    'data_type' => 'text'
                ],
                'created_at' => [
                    'data' => 'created_at',
                    'title' => trans('core::core.table.created_at'),
                    'data_type' => 'datetime'
                ],
                'updated_at' => [
                    'data' => 'updated_at',
                    'title' => trans('core::core.table.updated_at'),
                    'data_type' => 'datetime'
                ],
                'owner' => [
                    'data' => 'owner',
                    'title' => trans('core::core.table.assigned_to'),
                    'data_type' => 'assigned_to',
                    'orderable' => false
                ]
            ];

        if ($this->allowSelect) {
            return $check_selection + $columns;
        }
        if ($this->allowUnlink) {
            return $unlink + $columns;
        }

        return $columns;
    }
}

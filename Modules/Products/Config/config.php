<?php

return [
    'name' => 'Products',

    'entity_private_access' => false,

    /**
     * Always use lower name without custom characters, spaces, etc
     */
    'permissions' => [
        'products.settings',
        'products.browse',
        'products.create',
        'products.update',
        'products.destroy'
    ]
];

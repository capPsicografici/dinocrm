<?php

Route::group(['middleware' => ['web','permission:products.browse'],'prefix'=>'products','as'=>'products.', 'namespace' => 'Modules\Products\Http\Controllers'], function () {
    Route::get('/', function () {
        return redirect()->route('products.products.index');
    });

    Route::group(['middleware' => ['web','permission:products.settings']], function () {
        Route::resource('type', 'Settings\TypeController');
        Route::resource('category', 'Settings\CategoryController');
    });

    Route::resource('products', 'ProductsController');
});

<?php

namespace Modules\Products\Http\Controllers;

use App\Http\Requests\Request;
use Cog\Contracts\Ownership\Ownable;
use Modules\Core\Notifications\GenericNotification;
use Modules\Platform\Core\Http\Controllers\ModuleCrudController;
use Modules\Platform\Core\Repositories\GenericRepository;
use Modules\Platform\Notifications\Entities\NotificationPlaceholder;
use Modules\Platform\User\Entities\User;
use Modules\Products\Datatables\ProductDatatable;
use Modules\Products\Entities\Product;
use Modules\Products\Helper\ProductHelper;
use Modules\Products\Http\Forms\ProductForm;
use Modules\Products\Http\Requests\ProductsRequest;
use Intervention\Image\Facades\Image;

class ProductsController extends ModuleCrudController
{
    protected $datatable = ProductDatatable::class;
    protected $formClass = ProductForm::class;
    protected $storeRequest = ProductsRequest::class;
    protected $updateRequest = ProductsRequest::class;
    protected $entityClass = Product::class;

    protected $moduleName = 'products';

    protected $permissions = [
        'browse' => 'products.browse',
        'create' => 'products.create',
        'update' => 'products.update',
        'destroy' => 'products.destroy'
    ];

    protected $moduleSettingsLinks = [

        ['route' => 'products.type.index', 'label' => 'settings.type'],
        ['route' => 'products.category.index', 'label' => 'settings.category'],


    ];

    protected $jsFiles = [
        'BAP_Products.js'
    ];

    protected $settingsPermission = 'products.settings';

    protected $showFields = [

        'information' => [

            'name' => [
                'type' => 'text',
            ],

            'image_path' => ['type' => 'image'],

            'part_number' => [
                'type' => 'text',
            ],


            'vendor_part_number' => [
                'type' => 'text',
            ],


            'product_sheet' => [
                'type' => 'text',
            ],


            'website' => [
                'type' => 'text',
            ],


            'serial_no' => [
                'type' => 'text',
            ],


            'price' => [
                'type' => 'decimal',
            ],
            
            'owned_by' => [
                'type' => 'assigned_to',
            ],

            'vendor_id' => [
                'type' => 'manyToOne',
                'relation' => 'vendor',
                'column' => 'name',
                'dont_translate' => true
            ],


            'product_type_id' => [
                'type' => 'manyToOne',
                'relation' => 'productType',
                'column' => 'name'
            ],

            'product_category_id' => [
                'type' => 'manyToOne',
                'relation' => 'productCategory',
                'column' => 'name'
            ],

        ],


        'notes' => [

            'notes' => [
                'type' => 'text',
                'col-class' => 'col-lg-12'
            ],

        ],


    ];

    protected $languageFile = 'products::products';

    protected $routes = [
        'index' => 'products.products.index',
        'create' => 'products.products.create',
        'show' => 'products.products.show',
        'edit' => 'products.products.edit',
        'store' => 'products.products.store',
        'destroy' => 'products.products.destroy',
        'update' => 'products.products.update'
    ];

    public function __construct()
    {
        parent::__construct();
    }

    public function store()
    {
        $request = \App::make($this->storeRequest ?? Request::class);

        $mode = $request->get('entityCreateMode', self::FORM_MODE_FULL);

        if ($this->permissions['create'] != '' && !\Auth::user()->hasPermissionTo($this->permissions['create'])) {
            if ($mode == self::FORM_MODE_SIMPLE) {
                return response()->json([
                    'type' => 'error',
                    'message' => trans('core::core.entity.you_dont_have_access'),
                    'action' => 'show_message'
                ]);
            }
            flash(trans('core::core.you_dont_have_access'))->error();
            return redirect()->route($this->routes['index']);
        }

        $repository = $this->getRepository();

        $storeValues = $this->form($this->formClass)->getFieldValues(true);

        if ($mode == self::FORM_MODE_SIMPLE) {

            //Bind related element
            $relatedEntityId = $request->get('relatedEntityId');
            $relationType = $request->get('relationType', null);
            $relatedField = $request->get('relatedField');
            $relatedEntity = $request->get('relatedEntity');

            if ($relationType != null) { // Relation type is not null

                $relationEntityRepos = \App::make(GenericRepository::class);
                $relationEntityRepos->setupModel($relatedEntity);

                $relationEntity = $relationEntityRepos->findWithoutFail($relatedEntityId);

                if ($relationType == 'oneToMany') {
                    $storeValues[$relatedField] = $relationEntity->id;
                }
            }
        }


        $entity = $repository->createEntity($storeValues, \App::make($this->entityClass));


        $entity = $this->setupAssignedTo($entity, $request, true);
        $entity->save();


        //Product Image Upload
        $productImage = $request->file('image_path');

        if ($productImage != null) {

            $image = 'product_image_' . $entity->id . '_.' . $productImage->getClientOriginalExtension();

            $uploadSuccess = $productImage->move(ProductHelper::IMAGE_PATH, $image);

            if ($uploadSuccess) {
                $entity->image_path = ProductHelper::IMAGE_PATH.$image;
                $entity->save();
            }
        }

        if (config('bap.record_assigned_notification_enabled')) {

            if ($entity instanceof Ownable) {
                if ($entity->getOwner() != null && $entity->getOwner() instanceof User) {
                    if ($entity->getOwner()->id != \Auth::user()->id) { // Dont send notification for myself
                        try {
                            $commentOn = $entity->name;
                            $commentOn = ' - ' . $commentOn;
                        } catch (\Exception $exception) {
                            $commentOn = '';
                        }

                        $placeholder = new NotificationPlaceholder();

                        $placeholder->setRecipient($entity->getOwner());
                        $placeholder->setAuthorUser(\Auth::user());
                        $placeholder->setAuthor(\Auth::user()->name);
                        $placeholder->setColor('bg-green');
                        $placeholder->setIcon('assignment');
                        $placeholder->setContent(trans('notifications::notifications.new_record', ['user' => \Auth::user()->name]) . $commentOn);

                        $placeholder->setUrl(route($this->routes['show'], $entity->id));

                        $entity->getOwner()->notify(new GenericNotification($placeholder));
                    }
                }
            }
        }

        if ($mode == self::FORM_MODE_SIMPLE) {

            //Bind related element
            $relatedEntityId = $request->get('relatedEntityId');
            $relationType = $request->get('relationType', null);
            $relatedField = $request->get('relatedField');
            $relatedEntity = $request->get('relatedEntity');

            if ($relationType != null) { // Relation type is not null

                $relationEntityRepos = \App::make(GenericRepository::class);
                $relationEntityRepos->setupModel($relatedEntity);


                $relationEntity = $relationEntityRepos->findWithoutFail($relatedEntityId);


                if ($relationType == 'manyToMany') {
                    $entity->{$relatedField}()->attach($relationEntity->id);
                }
            }

            return response()->json([
                'type' => 'success',
                'message' => trans('core::core.entity.created'),
                'action' => 'refresh_datatable'
            ]);
        }

        flash(trans('core::core.entity.created'))->success();

        return redirect(route($this->routes['index']));
    }

    public function update($identifier)
    {
        if ($this->permissions['update'] != '' && !\Auth::user()->hasPermissionTo($this->permissions['update'])) {
            flash(trans('core::core.you_dont_have_access'))->error();
            return redirect()->route($this->routes['index']);
        }

        $request = \App::make($this->updateRequest ?? Request::class);

        $repository = $this->getRepository();

        $entity = $repository->find($identifier);

        $this->entity = $entity;

        if (empty($entity)) {
            flash(trans('core::core.entity.entity_not_found'))->error();

            return redirect(route($this->routes['index']));
        }

        if ($this->blockEntityOwnableAccess()) {
            flash(trans('core::core.you_dont_have_access'))->error();
            return redirect()->route($this->routes['index']);
        }

        $input = $this->form($this->formClass)->getFieldValues(true);

        $currentOwner = null;
        if ($entity instanceof Ownable && $entity->hasOwner()) {
            $currentOwner = $entity->getOwner();
        }

        $entity = $this->setupAssignedTo($entity, $input);

        $repository = $this->getRepository();

        //Product Image Upload
        $productImage = $request->file('image_path');

        if ($productImage != null) {

            $image = 'product_image_' . $identifier . '_.' . $productImage->getClientOriginalExtension();

            $uploadSuccess = $productImage->move(ProductHelper::IMAGE_PATH, $image);

            if ($uploadSuccess) {

                $input['image_path'] =  ProductHelper::IMAGE_PATH.$image;
            }
        }

        $entity = $repository->updateEntity($input, $entity);

        $this->entity = $entity;

        flash(trans('core::core.entity.updated'))->success();






        return redirect(route($this->routes['show'], $entity));
    }

}

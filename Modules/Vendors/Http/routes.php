<?php

Route::group(['middleware' => ['web','permission:vendors.browse'],'prefix'=>'vendors','as'=>'vendors.', 'namespace' => 'Modules\Vendors\Http\Controllers'], function () {
    Route::get('/', function () {
        return redirect()->route('vendors.vendors.index');
    });

    Route::group(['middleware' => ['web','permission:vendors.settings']], function () {
        Route::resource('category', 'Settings\CategoryController');
    });

    Route::resource('vendors', 'VendorsController');
});

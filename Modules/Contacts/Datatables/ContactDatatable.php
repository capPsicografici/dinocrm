<?php

namespace Modules\Contacts\Datatables;

use Modules\Contacts\Entities\Contact;
use Modules\Contacts\Entities\ContactStatus;
use Modules\Platform\Core\Datatable\PlatformDataTable;
use Modules\Platform\Core\Helper\DataTableHelper;
use Modules\Platform\Core\Helper\StringHelper;
use Yajra\DataTables\EloquentDataTable;

class ContactDatatable extends PlatformDataTable
{
    const SHOW_URL_ROUTE = 'contacts.contacts.show';

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);


        $this->applyLinks($dataTable, self::SHOW_URL_ROUTE);

        $dataTable->editColumn('status', function($record){
            return "<span class='badge ".StringHelper::badgeHelper($record->status_id,ContactStatus::COLORS)."'>$record->status</span>";
        });

        $dataTable->editColumn('phone', function($record){
            return "<span class='badge bg-grey'><i class='fa fa-mobile-phone'></i> $record->phone</span>";
        });
        $dataTable->editColumn('email', function($record){
            return "<span class='badge bg-grey'><i class='fa fa-envelope-o'></i> $record->email</span>";
        });

        $dataTable->filterColumn('owner', function ($query, $keyword) {
            DataTableHelper::queryOwner($query, $keyword, 'contacts');
        });

        $dataTable->filterColumn('created_at', function ($query, $keyword) {
            $dates = DataTableHelper::getDatesForFilter($keyword);

            if ($dates != null) {
                $query->whereBetween('contacts.created_at', array($dates[0], $dates[1]));
            }
        });
        $dataTable->filterColumn('updated_at', function ($query, $keyword) {
            $dates = DataTableHelper::getDatesForFilter($keyword);

            if ($dates != null) {
                $query->whereBetween('contacts.updated_at', array($dates[0], $dates[1]));
            }
        });


        return $dataTable;
    }

    /**
     * Get query source of dataTable.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Contact $model)
    {

        return $model->newQuery()
            ->with('owner')
            ->leftJoin('contacts_dict_status', 'contacts.contact_status_id', '=', 'contacts_dict_status.id')
            ->leftJoin('contacts_dict_source', 'contacts.contact_source_id', '=', 'contacts_dict_source.id')
            ->leftJoin('accounts', 'contacts.account_id', '=', 'accounts.id')
            ->select([
                'contacts.id',
                'contacts.full_name',
                'contacts.job_title',
                'contacts.department',
                'contacts.phone',
                'contacts.email',
                'contacts.city',
                'contacts.country',
                'contacts_dict_status.id as status_id',
                'contacts_dict_status.name as status',
                'contacts_dict_source.name as source',
                'accounts.name as account_name',
                'contacts.created_at as created_at',
                'contacts.updated_at as updated_at',
                'contacts.owned_by_id',
                'contacts.owned_by_type'
            ]);

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->setTableAttribute('class', 'table table-hover')
            ->parameters([
                'dom' => 'lBfrtip',

                'stateSave' => true,
                'columnFilters' => [
                    [
                        'column_number' => 0,
                        'filter_type' => 'text'
                    ],
                    [
                        'column_number' => 1,
                        'filter_type' => 'text'
                    ],
                    [
                        'column_number' => 2,
                        'filter_type' => 'text'
                    ],
                    [
                        'column_number' => 3,
                        'filter_type' => 'text'
                    ],
                    [
                        'column_number' => 4,
                        'filter_type' => 'text'
                    ],
                    [
                        'column_number' => 5,
                        'filter_type' => 'text'
                    ],
                    [
                        'column_number' => 6,
                        'filter_type' => 'text'
                    ],
                    [
                        'column_number' => 7,
                        'filter_type' => 'text'
                    ],
                    [
                        'column_number' => 8,
                        'filter_type' => 'text'
                    ],
                    [
                        'column_number' => 9,
                        'filter_type' => 'text'
                    ],
                    [
                        'column_number' => 10,
                        'filter_type' => 'bap_date_range_picker',

                    ],
                    [
                        'column_number' => 11,
                        'filter_type' => 'bap_date_range_picker',
                    ],
                    [
                        'column_number' => 12,
                        'filter_type' => 'select',
                        'select_type' => 'select2',
                        'select_type_options' => [
                            'theme' => "bootstrap",
                            'width' => '100%'
                        ],
                        'data' => DataTableHelper::filterOwnerDropdown()
                    ]
                ],
                'buttons' => DataTableHelper::buttons(),
                'regexp' => true

            ]);
    }

    /**
     * @return array
     */
    protected function getColumns()
    {
        return
            [
                'full_name' => [
                    'data' => 'full_name',
                    'title' => trans('core::core.table.name'),
                    'data_type' => 'text'
                ],
                'account' => [
                    'name' => 'accounts.name',
                    'data' => 'account_name',
                    'title' => trans('contacts::contacts.table.account'),
                    'data_type' => 'text'
                ],
                'status' => [
                    'name' => 'contacts_dict_status.name',
                    'data' => 'status',
                    'title' => trans('contacts::contacts.table.status'),
                    'data_type' => 'text'
                ],
                'source' => [
                    'name' => 'contacts_dict_source.name',
                    'data' => 'source',
                    'title' => trans('contacts::contacts.table.source'),
                    'data_type' => 'text'
                ],
                'job_title' => [
                    'name' => 'job_title',
                    'data' => 'job_title',
                    'title' => trans('contacts::contacts.table.job_title'),
                    'data_type' => 'text'
                ],
                'department' => [
                    'name' => 'department',
                    'data' => 'department',
                    'title' => trans('contacts::contacts.table.department'),
                    'data_type' => 'text'
                ],
                'phone' => [
                    'name' => 'phone',
                    'data' => 'phone',
                    'title' => trans('contacts::contacts.table.phone'),
                    'data_type' => 'text'
                ],
                'email' => [
                    'name' => 'email',
                    'data' => 'email',
                    'title' => trans('contacts::contacts.table.email'),
                    'data_type' => 'text'
                ],
                'city' => [
                    'name' => 'city',
                    'data' => 'city',
                    'title' => trans('contacts::contacts.table.city'),
                    'data_type' => 'text'
                ],
                'country' => [
                    'name' => 'country',
                    'data' => 'country',
                    'title' => trans('contacts::contacts.table.country'),
                    'data_type' => 'text'
                ],

                'created_at' => [
                    'data' => 'created_at',
                    'title' => trans('core::core.table.created_at'),
                    'data_type' => 'datetime'
                ],
                'updated_at' => [
                    'data' => 'updated_at',
                    'title' => trans('core::core.table.updated_at'),
                    'data_type' => 'datetime'
                ],
                'owner' => [
                    'data' => 'owner',
                    'title' => trans('core::core.table.assigned_to'),
                    'data_type' => 'assigned_to',
                    'orderable' => false
                ]
            ];
    }
}

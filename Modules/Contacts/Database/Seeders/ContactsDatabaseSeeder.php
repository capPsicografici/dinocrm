<?php

namespace Modules\Contacts\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Platform\Core\Helper\SeederHelper;

class ContactsDatabaseSeeder extends SeederHelper
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $contacts_dict_source = [
            ['id' => 1, 'name' => 'Cold call'],
            ['id' => 2, 'name' => 'Existing customer'],
            ['id' => 3, 'name' => 'Self generated'],
            ['id' => 4, 'name' => 'Employee/Partner'],
            ['id' => 5, 'name' => 'Public relations'],
            ['id' => 6, 'name' => 'Direct mail'],
            ['id' => 7, 'name' => 'Conference'],
            ['id' => 8, 'name' => 'Trade show'],
            ['id' => 9, 'name' => 'Web site'],
            ['id' => 10, 'name' => 'Word of mouth'],
            ['id' => 11, 'name' => 'Other'],
        ];

        DB::table('contacts_dict_source')->truncate();

        $this->saveOrUpdate('contacts_dict_source', $contacts_dict_source);

        $contacts_dict_status = [
            ['id' => 1, 'name' => 'New'],
            ['id' => 2, 'name' => 'Approved'],
            ['id' => 3, 'name' => 'In progress'],
            ['id' => 4, 'name' => 'Test'],
            ['id' => 5, 'name' => 'Trash'],
        ];

        DB::table('contacts_dict_status')->truncate();

        $this->saveOrUpdate('contacts_dict_status', $contacts_dict_status);
    }
}

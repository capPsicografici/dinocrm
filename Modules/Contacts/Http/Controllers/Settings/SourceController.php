<?php

namespace Modules\Contacts\Http\Controllers\Settings;

use Modules\Contacts\Datatables\Settings\ContactSourceDatatable;
use Modules\Contacts\Entities\ContactSource;
use Modules\Platform\Core\Http\Controllers\ModuleCrudController;
use Modules\Platform\Core\Http\Forms\NameDictionaryForm;
use Modules\Platform\Core\Http\Requests\NameDictionaryRequest;

class SourceController extends ModuleCrudController
{
    protected $datatable = ContactSourceDatatable::class;
    protected $formClass = NameDictionaryForm::class;
    protected $storeRequest = NameDictionaryRequest::class;
    protected $updateRequest = NameDictionaryRequest::class;
    protected $entityClass = ContactSource::class;

    protected $moduleName = 'contacts';

    protected $settingsBackRoute = 'contacts.contacts.index';

    protected $showFields = [
        'details' => [
            'name' => ['type' => 'text'],
        ]
    ];

    protected $languageFile = 'contacts::contacts.source';

    protected $routes = [
        'index' => 'contacts.source.index',
        'create' => 'contacts.source.create',
        'show' => 'contacts.source.show',
        'edit' => 'contacts.source.edit',
        'store' => 'contacts.source.store',
        'destroy' => 'contacts.source.destroy',
        'update' => 'contacts.source.update'
    ];

    public function __construct()
    {
        parent::__construct();
    }
}

<?php

namespace Modules\Contacts\Http\Requests;

use App\Http\Requests\Request;

/**
 * Class ContactsRequest
 * @package Modules\Contacts\Http\Requests
 */
class ContactsRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'secondary_email' => 'sometimes|nullable|email',
        ];
    }
}

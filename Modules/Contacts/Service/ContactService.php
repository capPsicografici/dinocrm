<?php

namespace Modules\Contacts\Service;

use HipsterJazzbo\Landlord\Facades\Landlord;
use Modules\Contacts\Entities\Contact;

/**
 * Class ContactService
 * @package Modules\Contacts\Service
 */
class ContactService
{

    /**
     * Count contact by status
     * @param $status
     * @return mixed
     */
    public function countByStatus($status)
    {
        $contacts = Contact::where('contact_status_id', $status);

        if (Landlord::hasTenant('company_id')) {
            $contacts->where('company_id', Landlord::getTenantId('company_id'));
        }

        return $contacts->count();
    }
}

<?php

namespace Modules\Campaigns\Datatables;

use Modules\Campaigns\Entities\Campaign;
use Modules\Campaigns\Entities\CampaignStatus;
use Modules\Platform\Core\Datatable\PlatformDataTable;
use Modules\Platform\Core\Helper\DataTableHelper;
use Modules\Platform\Core\Helper\StringHelper;
use Yajra\DataTables\EloquentDataTable;

class CampaignDatatable extends PlatformDataTable
{
    const SHOW_URL_ROUTE = 'campaigns.campaigns.show';

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);



        $this->applyLinks($dataTable, self::SHOW_URL_ROUTE);

        $dataTable->filterColumn('owner', function ($query, $keyword) {
            DataTableHelper::queryOwner($query, $keyword,'campaigns');
        });

        $dataTable->editColumn('status', function($record){
            return "<span class='badge ".StringHelper::badgeHelper($record->status_id,CampaignStatus::COLORS)."'>$record->status</span>";
        });

        $dataTable->filterColumn('expected_close_date', function ($query, $keyword) {
            $dates = DataTableHelper::getDatesForFilter($keyword);

            if ($dates != null) {
                $query->whereBetween('campaigns.expected_close_date', array($dates[0], $dates[1]));
            }
        });
        $dataTable->filterColumn('created_at', function ($query, $keyword) {
            $dates = DataTableHelper::getDatesForFilter($keyword);

            if ($dates != null) {
                $query->whereBetween('campaigns.created_at', array($dates[0], $dates[1]));
            }
        });
        $dataTable->filterColumn('updated_at', function ($query, $keyword) {
            $dates = DataTableHelper::getDatesForFilter($keyword);

            if ($dates != null) {
                $query->whereBetween('campaigns.updated_at', array($dates[0], $dates[1]));
            }
        });


        return $dataTable;
    }

    /**
     * Get query source of dataTable.
     *
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Campaign $model)
    {
        return $model->with('owner')
            ->leftJoin('campaigns_dict_status', 'campaigns.campaign_status_id', '=', 'campaigns_dict_status.id')
            ->leftJoin('campaigns_dict_type', 'campaigns.campaign_type_id', '=', 'campaigns_dict_type.id')
            ->newQuery()->select([
                'campaigns.id',
                'campaigns.name',
                'campaigns.product',
                'campaigns_dict_status.id as status_id',
                'campaigns_dict_status.name as status',
                'campaigns_dict_type.name as type',
                'campaigns.expected_close_date',
                'campaigns.sponsor',
                'campaigns.created_at as created_at',
                'campaigns.updated_at as updated_at',
                'campaigns.owned_by_id',
                'campaigns.owned_by_type'
            ]);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())

            ->setTableAttribute('class', 'table table-hover')
            ->parameters([
                'dom' => 'lBfrtip',
                'stateSave' => true,
                'responsive' => false,
                'columnFilters' => [
                    [
                        'column_number' => 0,
                        'filter_type' => 'text'
                    ],
                    [
                        'column_number' => 1,
                        'filter_type' => 'text'
                    ],
                    [
                        'column_number' => 2,
                        'filter_type' => 'text'
                    ],
                    [
                        'column_number' => 3,
                        'filter_type' => 'text'
                    ],
                    [
                        'column_number' => 4,
                        'filter_type' => 'bap_date_range_picker',

                    ],
                    [
                        'column_number' => 5,
                        'filter_type' => 'text'
                    ],
                    [
                        'column_number' => 6,
                        'filter_type' => 'bap_date_range_picker',

                    ],
                    [
                        'column_number' => 7,
                        'filter_type' => 'bap_date_range_picker',
                    ],
                    [
                        'column_number' => 8,
                        'filter_type' => 'select',
                        'select_type' => 'select2',
                        'select_type_options' => [
                            'theme' => "bootstrap",
                            'width' => '100%'
                        ],
                        'data' => DataTableHelper::filterOwnerDropdown()
                    ]
                ],
                'buttons' => DataTableHelper::buttons(),
                'regexp' => true

            ]);
    }

    /**
     * @return array
     */
    protected function getColumns()
    {



        return
            [
                'name' => [
                    'data' => 'name',
                    'title' => trans('core::core.table.name'),
                    'data_type' => 'text',
                ],
                'product' => [
                    'data' => 'product',
                    'title' => trans('campaigns::campaigns.table.product'),
                    'data_type' => 'text',
                ],
                'status' => [
                    'name' => 'campaigns_dict_status.name',
                    'data' => 'status',
                    'title' => trans('campaigns::campaigns.table.status'),
                    'data_type' => 'text'
                ],
                'type' => [
                    'name' => 'campaigns_dict_type.name',
                    'data' => 'type',
                    'title' => trans('campaigns::campaigns.table.type'),
                    'data_type' => 'text'
                ],
                'expected_close_date' => [
                    'data' => 'expected_close_date',
                    'title' => trans('campaigns::campaigns.table.expected_close_date'),
                    'data_type' => 'datetime',
                ],
                'sponsor' => [
                    'data' => 'sponsor',
                    'title' => trans('campaigns::campaigns.table.sponsor'),
                    'data_type' => 'text',
                ],
                'created_at' => [
                    'data' => 'created_at',
                    'title' => trans('core::core.table.created_at'),
                    'data_type' => 'datetime'
                ],
                'updated_at' => [
                    'data' => 'updated_at',
                    'title' => trans('core::core.table.updated_at'),
                    'data_type' => 'datetime'
                ],
                'owner' => [
                    'data' => 'owner',
                    'title' => trans('core::core.table.assigned_to'),
                    'data_type' => 'assigned_to',
                    'orderable' => false
                ]
            ];
    }
}

<?php

namespace Modules\Platform\Companies\Repositories;

use Modules\Platform\Companies\Entities\Company;
use Modules\Platform\Core\Repositories\PlatformRepository;


/**
 * Class CompanyRepository
 * @package Modules\Platform\Companies\Repositories
 */
class CompanyRepository extends PlatformRepository
{
    public function model()
    {
        return Company::class;
    }
}

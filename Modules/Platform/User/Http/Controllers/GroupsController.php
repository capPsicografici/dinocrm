<?php

namespace Modules\Platform\User\Http\Controllers;

use App\Http\Requests\Request;
use Modules\Platform\Companies\Datatables\Scope\CurrentCompanyScope;
use Modules\Platform\Core\Http\Controllers\SettingsCrudController;
use Modules\Platform\User\Datatables\GroupsDatatable;
use Modules\Platform\User\Entities\User;
use Modules\Platform\User\Http\Forms\GroupForm;
use Modules\Platform\User\Http\Requests\GroupCreateRequest;
use Modules\Platform\User\Http\Requests\GroupUpdateRequest;
use Modules\Platform\User\Repositories\GroupRepository;
use Modules\Platform\User\Repositories\UserRepository;

class GroupsController extends SettingsCrudController
{
    protected $datatable = GroupsDatatable::class;
    protected $formClass = GroupForm::class;
    protected $storeRequest = GroupCreateRequest::class;
    protected $updateRequest = GroupUpdateRequest::class;
    protected $repository = GroupRepository::class;


    protected $showFields = [
        'details' => [
            'name' => ['type' => 'text'],
        ],
        'users' => [
            'users' => ['type' => 'manyToMany', 'relation' => 'users', 'column' => 'name',],
        ]
    ];

    protected $languageFile = 'user::groups';

    protected $routes = [
        'index' => 'settings.groups.index',
        'create' => 'settings.groups.create',
        'show' => 'settings.groups.show',
        'edit' => 'settings.groups.edit',
        'store' => 'settings.groups.store',
        'destroy' => 'settings.groups.destroy',
        'update' => 'settings.groups.update'
    ];

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $datatable = \App::make($this->datatable);

        $datatable->addScope(new CurrentCompanyScope());

        $indexView = $this->views['index'];

        return $datatable->render($indexView);
    }

    public function show($identifier)
    {
        if(!\Auth::user()->hasPermissionTo('settings.access')){
            $entity = $request = \App::make($this->repository)->findWithoutFail($identifier);
            if($entity->company_id != \Auth::user()->company_id){
                flash(trans($this->languageFile . '.unaproved'))->error();
                return redirect(route($this->routes['index']));
            }
        }

        return parent::show($identifier);
    }

    public function edit($identifier)
    {
        if(!\Auth::user()->hasPermissionTo('settings.access')){
            $entity = $request = \App::make($this->repository)->findWithoutFail($identifier);
            if($entity->company_id != \Auth::user()->company_id){
                flash(trans($this->languageFile . '.unaproved'))->error();
                return redirect(route($this->routes['index']));
            }
        }

        return parent::edit($identifier);
    }

    public function destroy($identifier)
    {
        if(!\Auth::user()->hasPermissionTo('settings.access')){
            $entity = $request = \App::make($this->repository)->findWithoutFail($identifier);
            if($entity->company_id != \Auth::user()->company_id){
                flash(trans($this->languageFile . '.unaproved'))->error();
                return redirect(route($this->routes['index']));
            }
        }

        return parent::destroy($identifier);
    }

    /**
     * Store entity
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store()
    {
        $loggedUser = \Auth::user();

        $request = \App::make($this->storeRequest ?? Request::class);

        $entity = \App::make($this->repository)->create($request->all());

        if (is_null($request->get('users'))) {
            $entity->users()->sync([]);
        } else {

            $userRepo = \App::make(UserRepository::class);

            $users = $request->get('users');
            foreach ($users as $user){
                $u = $userRepo->findWithoutFail($user);
                if(empty($u)){
                    flash(trans($this->languageFile . '.unaproved'))->error();
                    return redirect(route($this->routes['index']));
                }
                if(!$loggedUser->isAdmin() && $u->company_id != $loggedUser->company_id){
                    flash(trans($this->languageFile . '.unaproved'))->error();
                    return redirect(route($this->routes['index']));
                }
            }

            $entity->users()->sync($users);
        }

        flash(trans($this->languageFile . '.created'))->success();

        return redirect(route($this->routes['index']));
    }

    /**
     * Update entity
     * @param $identifier
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($identifier)
    {
        $loggedUser = \Auth::user();

        $request = \App::make($this->updateRequest ?? Request::class);

        $repository = \App::make($this->repository);

        $entity = $repository->find($identifier);

        if(!\Auth::user()->hasPermissionTo('settings.access')){

            if($entity->company_id != \Auth::user()->company_id){
                flash(trans($this->languageFile . '.unaproved'))->error();
                return redirect(route($this->routes['index']));
            }
        }

        if (empty($entity)) {
            flash(trans($this->languageFile . '.entity_not_found'))->success();

            return redirect(route($this->routes['index']));
        }

        $entity = \App::make($this->repository)->update($request->all(), $identifier);


        if (is_null($request->get('users'))) {
            $entity->users()->sync([]);
        } else {
            $userRepo = \App::make(UserRepository::class);

            $users = $request->get('users');
            foreach ($users as $user){
                $u = $userRepo->findWithoutFail($user);
                if(empty($u)){
                    flash(trans($this->languageFile . '.unaproved'))->error();
                    return redirect(route($this->routes['index']));
                }
                if(!$loggedUser->isAdmin() && $u->company_id != $loggedUser->company_id){
                    flash(trans($this->languageFile . '.unaproved'))->error();
                    return redirect(route($this->routes['index']));
                }
            }

            $entity->users()->sync($users);
        }

        flash(trans($this->languageFile . '.updated'))->success();

        return redirect(route($this->routes['show'], $entity));
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: jw
 * Date: 02.10.18
 * Time: 09:59
 */

namespace Modules\Platform\Core\Entities;


use Illuminate\Database\Eloquent\Model;

/**
 * Temporary table
 *
 * Class CsvData
 * @package Modules\Platform\Core\Entities
 */
class CsvData extends Model
{

    protected $table = 'bap_csv_data';

    protected $fillable = [
        'csv_filename',
        'csv_header',
        'csv_data'
    ];

}
